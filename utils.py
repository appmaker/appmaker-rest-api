import datetime
import time
import base64
import json
import magic
import logging


class Utils(object):

    def __init__(self, parent):
        self.parent = parent
        self._deployments = {
            "appmaker": {
                "type": "appmaker",
                "params": {
                    "subdomain": ""
                }
            },
            "github": {
                "params": {
                    "branch": "miurlparaghpages"
                },
                "type": "github"
            },
            "ssh": {
                "type": "ssh",
                "params": {
                    "deploymentZipFileName": "afile.zip",
                    "deploymentNotificationWebHook": "da_hook"
                }
            }
        }
        self.logger = logging.getLogger(self.__class__.__name__)

    def create_project(self, url, payload, expected_status=202):
        self.logger.debug("Sending post to URL: {}".format(url))
        res = self.parent.send_request(
            url, 'post', payload, expected_status=expected_status, expected_values=['projectId'],
            expected_headers=['location'])
        self.logger.debug("Create project response: {}".format(res))
        if 'json' in res.headers['content-type'].split("/"):
            return res.json()['projectId'], res.headers['location']
        else:
            return res.status_code, res.content

    def delete_project(self, url, expected_status=202):
        res = self.parent.send_request(url, 'delete', None, expected_status=expected_status)
        json = res.json()
        return res.headers['location'] if not 'error' in json else json

    def delete_all_projects(self):
        res = self.parent.send_request(self.parent.root + 'projects', 'get', None, expected_status=200)
        projects = res.json()
        for proj in projects:
            project_id = proj['projectId']
            del_res = self.parent.send_request(self.parent.root + 'projects/' + project_id, 'delete', None, 202)
            work_id = del_res.headers['location']
            self.wait_for_worker_progress(work_id, 100, 20)

    def create_resource(self, url, resource_path, expected_status=202):
        '''
            Create a origin
        '''
        payload = {'type': 'zip'}
        files = [('src', (resource_path.split('/')[-1], open(resource_path, 'rb'), 'application/octect-stream'))]

        res = self.parent.send_request(url, 'post', payload, files=files,
                                       expected_status=expected_status, expected_values=['originId'],
                                       expected_headers=['location'])
        return res.json()['originId'], res.headers['location']

    def create_channel(self, url, expected_status=202, **kwargs):
        '''
            Creates a channel within an existing project

            :param url: URL of the project where the channel will be created
            :param channel_name: type of the channel (BETA, PRODUCTION)
            :param origin_id: id of the zip previously uploaded to origins/
            :param deployment_type: deployment mechanism (github, ssh,...)
            :param app_info: a dictionary which contains the name and description of the app,
            and the relative path to the icon.

            :returns: the brand new channel id, the location of the associate worker
        '''
        # Let us build the payload
        payload = {}
        payload.update(kwargs)

        # Changes needed for the server to accept this request (icon to base64, json dump, correct deployment format)
        if 'appInfo' in payload and 'iconUrl' in payload['appInfo']:
            mime_type = magic.from_file(payload['appInfo']['iconUrl'], mime=True)
            payload['appInfo']['iconUrl'] = "data:{};base64,".format(mime_type) + \
                                            self.icon_to_base64(payload['appInfo']['iconUrl'])

        if 'deployment' in payload:
            payload['deployment'] = self._deployments.get(payload['deployment'])
            self.logger.debug("deployment: %s" % (payload.get('deployment', {})))
            if 'deployment' in payload and payload['deployment'] and \
                    'subdomain' in payload.get('deployment', {}).get('params', {}):
                payload['deployment']['params']['subdomain'] = "test{:.0f}".format(time.time())

        payload['tag'] = payload['name'].lower()

        res = self.parent.send_request(url, 'post', json.dumps(payload), expected_status=expected_status,
                                       expected_values=['channelId'], expected_headers=['location'],
                                       content_type='application/json; charset=utf-8')

        if 'json' in res.headers['content-type'].split("/"):
            return res.json()['channelId'], res.headers['location']
        else:
            return res.status_code, res.content

    def check_channel_field(self, field_name, original, modified):
        """
        Check if the original field value for the field_name equals the
        modified field value, and asserts the equality.
        If the values don't match, an exception is thrown.
        """
        self.parent.assertEquals(original, modified, 'New {} [{}] is not as expected [{}]'.
                          format(field_name, modified, original))

    def icon_to_base64(self, icon_path):
        with open(icon_path, "rb") as icon_file:
            encoded_string = base64.b64encode(icon_file.read())
        return encoded_string

    def _get_deployment(self, deployment_type):
        invalid = {
                'name': "invalidDeployment",
                'params': {
                    'invalidParam': "invalidValue"
                }
            }
        return {self._deployments.get(deployment_type, invalid)}

    def wait_for_worker_progress(self, location, progress, timeout):
        self.logger.debug("Cheking worker with location: {}".format(location))
        url = '{}{}'.format(self.parent.server_root, location)
        starttime = datetime.datetime.now()
        while datetime.datetime.now() - starttime < datetime.timedelta(seconds=timeout):
            res = self.parent.send_request(url, 'get', None, expected_status=200, expected_values=['progress'])
            if res.json()['progress'] >= progress:
                return True
            time.sleep(1)
        return False
