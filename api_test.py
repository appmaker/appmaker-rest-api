# -*- coding: utf-8 -*-
'''
(c) Copyright 2014 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

import unittest
import logging.config
import requests
from utils import Utils


class TestAPI(unittest.TestCase):

    def setUp(self):
        self.server_root = 'http://test-appmaker.tid.es:1863'
        self.root = self.server_root + '/appmaker/v1/'
        logging.config.fileConfig('conf/logger.cfg')
        self.logger = logging.getLogger(self.__class__.__name__)
        self.utils = Utils(self)
        self.logger.debug("---------- {} -----------".format(self.__class__.__name__))

    def tearDown(self):
        pass

    def send_request(self, url, method, payload, expected_status, files=None, expected_values=[], expected_headers=[],
                     content_type=None):
        """Send a generic REST request.

        Send a REST request to the given URL, invoking the specified method.
        """
        m = getattr(requests, method)
        self.logger.debug("Sending {} request to {}".format(method.upper(), url))
        self.logger.debug("payload: {}".format(payload))
        self.logger.debug("files: {}".format(files))
        h = {'content-type': content_type} if content_type is not None else None
        resp = m(url, data=payload, headers=h, files=files)
        self.logger.debug("Response status code: {}".format(resp.status_code))
        self.logger.debug("Response body: [{}]".format(resp.content))
        self.logger.debug("Response headers: {}".format(resp.headers))
        self.assertEquals(resp.status_code, expected_status, "Expected status code: {}  Got: {}".
                        format(expected_status, resp.status_code))
        self.logger.debug("Content type: {}".format(resp.headers['Content-Type']))

        if 'json' in resp.headers['content-type']:
            r = resp.json()
            self.logger.debug("Response received: {}".format(r))
            self.logger.debug("Expected values in response: {}".format(expected_values))
            for value in expected_values:
                self.assertIn(value, r, "Expected value {} was not found in the response".format(value))
            for header in expected_headers:
                self.assertIn(header, resp.headers, "Expected header {} was not found in the response".format(header))
        return resp
