# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

#===============================================================================
# [GET] Project list, at least one project already created
#===============================================================================
from api_test import TestAPI


class TestOWDAppMaker294(TestAPI):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.resource = 'projects'
        self.expected_status = 200
        url = self.root + self.resource
        payload = {'name': 'Project name'}
        self.project_id, work_id = self.utils.create_project(url, payload)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg='Project not created')

    def tearDown(self):
        url = '{}{}/{}'.format(self.root, self.resource, self.project_id)
        work_id = self.utils.delete_project(url)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), "Project not deleted")

    def test_get_projects(self):
        url = self.root + self.resource
        self.logger.debug('Sending get to URL: {}'.format(url))
        r = self.send_request(url, 'get', None, self.expected_status)
        self.assertGreater(len(r.json()), 0, msg='At least one project was expected in the response')
