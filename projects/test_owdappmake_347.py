# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

#===============================================================================
# [GET] Get a project's channels
#===============================================================================
from api_test import TestAPI


class TestOWDAppMaker347(TestAPI):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.resource = 'projects'
        self.expected_status = 200
        payload = {'name': 'Test App cool name'}
        self.url = self.root + self.resource
        self.project_id, work_id = self.utils.create_project(self.url, payload)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg='Project not created')

    def tearDown(self):
        url = '{}{}/{}'.format(self.root, self.resource, self.project_id)
        work_id = self.utils.delete_project(url)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), "Project not deleted")

    def test_get_channels(self):
        url = '{}{}/{}'.format(self.root, self.resource, self.project_id)
        r = self.send_request(url, 'get', None, self.expected_status)

        # Checking that channels is empty
        chan = r.json()['channels']
        self.assertEquals(len(chan), 0, "No channels were expected to be found")
