# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

#===============================================================================
# [PUT] Data modification, no data changes
#===============================================================================
from api_test import TestAPI


class TestOWDAppMaker299(TestAPI):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.resource = 'projects'
        self.expected_status = 202
        url = self.root + self.resource
        self.payload = {'name': 'Test App cool name'}
        self.project_id, work_id = self.utils.create_project(url, self.payload)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg='Project not created')

    def tearDown(self):
        url = '{}{}/{}'.format(self.root, self.resource, self.project_id)
        work_id = self.utils.delete_project(url)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), "Project not deleted")

    def test_modify_project(self):
        url = '{}{}/{}'.format(self.root, self.resource, self.project_id)
        self.logger.debug("Sending put to URL: {}".format(url))
        r = self.send_request(url, 'put', self.payload, self.expected_status, expected_headers=['location'])
        self.assertEquals(r.status_code, self.expected_status, "Expected status code: {}  Got: {}".
                        format(self.expected_status, r.status_code))

        work_id = r.headers['location']
        self.logger.debug("Checking work with id: {}".format(work_id))

        self.assertTrue(self.utils.wait_for_worker_progress(
            work_id, progress=100, timeout=20), msg='Project not modified. Put failed')

        # Check project data is the same as before the PUT request
        url = self.root + self.resource + '/{}'.format(self.project_id)
        r = self.send_request(url, 'get', None, expected_status=200, expected_values=['name'])
        self.assertEquals(r.json()['name'], self.payload['name'],
                          msg='Project name does not match the new given value')
