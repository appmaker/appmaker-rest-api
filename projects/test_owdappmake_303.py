# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

#===============================================================================
# [DELETE] Delete project, project does not exist
#===============================================================================

from api_test import TestAPI


class TestOWDAppMaker303(TestAPI):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.resource = 'projects'

    def test_delete_project(self):
        url = self.root + self.resource + '/{}'.format('invalidprojectid')
        self.utils.delete_project(url, expected_status=404)
