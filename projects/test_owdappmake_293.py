# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

#===============================================================================
# [GET] Project list, no project(s) created yet
#===============================================================================
from api_test import TestAPI


class TestOWDAppMaker293(TestAPI):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.resource = 'projects'
        self.expected_status = 200

    def test_get_projects(self):
        url = self.root + self.resource
        self.logger.debug('Sending get to URL: {}'.format(url))
        r = self.send_request(url, 'get', None, self.expected_status)
        self.assertEquals(len(r.json()), 0, msg='No projects were expected to be found')
