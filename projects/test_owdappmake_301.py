# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

#===============================================================================
# [PUT] Data modification, name
#===============================================================================
from api_test import TestAPI


class TestOWDAppMaker301(TestAPI):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.resource = 'projects'
        self.expected_status = 202
        url = self.root + self.resource
        payload = {'name': 'Test App cool name'}
        self.project_id, work_id = self.utils.create_project(url, payload)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg='Project not created')

    def tearDown(self):
        url = '{}{}/{}'.format(self.root, self.resource, self.project_id)
        work_id = self.utils.delete_project(url)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), "Project not deleted")

    def test_modify_project(self):
        url = self.root + self.resource + '/{}'.format(self.project_id)
        new_name = 'New name'
        payload = {'name': new_name}
        resp = self.send_request(url, 'put', payload, self.expected_status)
        work_id = resp.headers['location']
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg='Project not modified. Put failed')

        resp = self.send_request(url, 'get', payload=None, expected_status=200, expected_values=['projectId'])
        proj_name = resp.json()['name']
        self.assertEquals(proj_name, new_name, "The app name '{}' is not the expected '{}'".
                        format(proj_name, new_name))
