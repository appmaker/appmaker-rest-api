# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

#===============================================================================
# [PUT] Data modification, project does not exist
#===============================================================================
from api_test import TestAPI
from seleniumtid.jira import jira


class TestOWDAppMaker300(TestAPI):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.resource = 'projects'
        self.expected_status = 404

    def test_modify_project(self):
        url = self.root + self.resource + '/invalidprojectid'
        payload = {'name': 'new name'}
        self.send_request(url, 'put', payload, self.expected_status)
