# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

#===============================================================================
# [PUT] Data modification, name already exists
#===============================================================================
from api_test import TestAPI


class TestOWDAppMaker884(TestAPI):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.resource = 'projects'
        self.expected_status = 202
        url = self.root + self.resource
        payload = {'name': 'Test project 884-1'}
        self.project_id_1, work_id = self.utils.create_project(url, payload)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg='Project 884-1 not created')
        payload = {'name': 'Test project 884-2'}
        self.project_id_2, work_id = self.utils.create_project(url, payload)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg='Project 884-2 not created')

    def tearDown(self):
        url = '{}{}/{}'.format(self.root, self.resource, self.project_id_1)
        work_id = self.utils.delete_project(url)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), "Project 884-1 not deleted")
        url = '{}{}/{}'.format(self.root, self.resource, self.project_id_2)
        work_id = self.utils.delete_project(url)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), "Project 884-2 not deleted")

    def test_modify_project(self):
        url = self.root + self.resource + '/{}'.format(self.project_id_2)
        payload = {'name': 'Test project 884-1'}
        self.send_request(url, 'put', payload, expected_status=409)
