# -*- coding: utf-8 -*-
'''
(c) Copyright 2014 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
import requests
from api_test import TestAPI


class TestGetRoot(TestAPI):

    def test_get_root(self):
        '''
        Get the AppMaker root page (should return 'resource not found')
        '''
        expected_status_code = 404
        expected_error_message = 'Resource not found'
        res = requests.get(self.root)

        print "Response - status code: {}".format(res.status_code)
        print "Response - content: {}".format(res.json())
        self.assertEquals(expected_status_code, res.status_code)
        self.assertEquals(expected_error_message, res.json()['error'])
