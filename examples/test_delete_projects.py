# -*- coding: utf-8 -*-
'''
(c) Copyright 2014 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
from api_test import TestAPI
import requests
import uuid


class TestPostProject(TestAPI):

    def setUp(self):
        super(TestPostProject, self).setUp()
        self.resource = 'projects'
        # Create the project which will be deleted
        proj_uuid = str(uuid.uuid4())[-8:]
        payload = {'name': 'test_project_{}'.format(
            proj_uuid), 'description': 'test_description', 'callbackUrl': 'http://tid.es'}

        # files = [('icon', ('icon.png', open('res/icon.png', 'rb'), 'image/png'))]
        res = requests.post(self.root + self.resource, data=payload)  # , files=files)
        self.proj_id = res.json()["projectId"]
        print "Project ID: {}".format(self.proj_id)

    def tearDown(self):
        super(TestPostProject, self).tearDown()

    def test_delete_project(self):
        '''
            DELETE - http://server/appmaker/v1/projects/:projectId
        '''
        # curl -v -X DELETE
        # "http://appmaker.tid.ovh:8080/appmaker/v1/projects/0419e18314cea6fb887f27d4bb1c519d526299b9"
        res = requests.delete(self.root + 'projects/{}'.format(self.proj_id))
        print "Response - status code: {}".format(res.status_code)
        print "Response - content: {}".format(res.json())
