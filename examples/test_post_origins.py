# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
from api_test import TestAPI
import requests


class TestPostOrigin(TestAPI):

    def setUp(self):
        super(TestPostOrigin, self).setUp()
        self.resource = 'origins'

    def tearDown(self):
        # TODO - Insert code to delete project here
        super(TestPostOrigin, self).tearDown()

    def test_create_new_origin(self):
        '''
        Create a new project
        POST
            Provide and analyze a web app of a developer
            request
                type=<zip | gh>  (http://server/appmaker/v1/originTypes)
                src=<zipFile or github URL>
            response 202 (Accepted)
                originId
                workId
        '''
        path = '/home/fran/owd/owd-obcertification/resources/zips/test-appcache.zip'

        url = self.root + self.resource
        print url
        r = requests.post(url, data={'type': 'zip'}, files={'src': ('test-appcache.zip', open(path, 'rb'))})

        print "Response text: {}".format(r.text)
        print "Response - status code: {}".format(r.status_code)
        print "Response - content: {}".format(r.json())
