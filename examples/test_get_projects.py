# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
import requests
from api_test import TestAPI


class TestGetProjects(TestAPI):

    def setUp(self):
        super(TestGetProjects, self).setUp()
        self.resource = 'projects'

    def test_get_projects(self):
        '''
        GET
        List all projects of a developer
        '''
        res = requests.get(self.root + self.resource)

        print "Response: {}".format(res)
        print "Response text: {}".format(res.text)
        print "Response status code: {}".format(res.status_code)
        print "Response - json: {}".format(res.json())
