# -*- coding: utf-8 -*-
'''
(c) Copyright 2014 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
from api_test import TestAPI
import requests
import uuid


class TestPostProject(TestAPI):

    def setUp(self):
        super(TestPostProject, self).setUp()
        self.resource = 'projects'

    def tearDown(self):
        #TODO - Insert code to delete project here
        super(TestPostProject, self).tearDown()

    def test_create_new_project(self):
        '''
        Create a new project
        POST
            request
                name=<prjName>
                description=<prjDescription>
                icon=<icon>
                callbackUrl=<callbackUrl> - for notifications
            response (if exists a manifest)
                projectId=<prjId>
                workId=<workId>
        '''
        proj_uuid = str(uuid.uuid4())[-8:]
        print "Project name: {}".format(proj_uuid)
        payload = {'name': 'test_project_{}'.format(
            proj_uuid), 'description': 'test_description', 'callbackUrl': 'http://tid.es'}

        # curl - v - X POST - F "name=test2" - F "description=PruebaDesdecURL" - F "callbackUrl=http://tid.es"
        # - F "icon=@res/icon.png" "http://appmaker.tid.ovh:8080/appmaker/v1/projects"
        # files = [('icon', ('icon.png', open('res/icon.png', 'rb'), 'image/png'))]

        res = requests.post(self.root + self.resource, data=payload)  # , files=files)

        print "Response text: {}".format(res.text)
        print "Response - status code: {}".format(res.status_code)
        print "Response - content: {}".format(res.json())

