# BetterMobileWeb Server Testing
This is the server test automation of the BetterMobileWeb suite (named internally as AppMaker).

There are more information regarding the suite in following links:
  * [Product Backlog](https://docs.google.com/document/d/1Ue29Yzjz0j6f-DwxJPaGEOfs5fJq9JExSv7Mo7ehEzY/edit?usp=sharing)
  * [User Experience](https://drive.google.com/folderview?id=0ByQxzecMNRSSflVtNnd6TmlKOGZpMV9aWVRsUHhCWkdjY0pWcThlenNBN0Nhc1pPRFU2UGM&usp=sharing)
  * [Test Cases](https://docs.google.com/spreadsheets/d/19AoBMVw9YnlKzaxQLFIg0fpcBJekGNOfNuxRi7eRyCw/edit?usp=sharing)
  * [Testing Guide](https://docs.google.com/document/d/1xsEhJb6aMeJuR9npH97J2Ek2s1LUbwiIK4dGFIT-r_Q/edit?usp=sharing)

See also the other repositories of the suite: [server side](https://gitlab.com/appmaker/server), [client side](https://gitlab.com/appmaker/client) and [end2end testing](https://gitlab.com/appmaker/selenium-appium).