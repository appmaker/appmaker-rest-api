# -*- coding: utf-8 -*-

# [POST] Upload the same zip file twice

'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
from api_test import TestAPI


class TestOWDAppMaker291(TestAPI):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.resource = 'origins'
        self.expected_status = 201

    def test_create_new_origin_twice(self):
        path = 'res/test-appcache.zip'
        url = self.root + self.resource

        origin_id_first, work_id_first = self.utils.create_resource(url, path)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id_first, 100, 20), msg='Origin #1 not created')

        origin_id_second, work_id_second = self.utils.create_resource(url, path)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id_second, 100, 20), msg='Origin #2 not created')

        self.assertEquals(origin_id_first, origin_id_second, msg='Origin Ids are not the same: {} - {}'.
                          format(origin_id_first, origin_id_second))
