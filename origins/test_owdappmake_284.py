# -*- coding: utf-8 -*-

# [POST] Upload a zip

'''
(c) Copyright 2014 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
from api_test import TestAPI
from seleniumtid.jira import jira


class TestOWDAppMaker284(TestAPI):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.resource = 'origins'

    def test_upload_a_zip(self):
        path = 'res/test-appcache.zip'
        url = self.root + self.resource

        _, work_id = self.utils.create_resource(url, path)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg="Origin not created")
