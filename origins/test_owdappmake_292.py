# -*- coding: utf-8 -*-

# [GET] Uploaded resource

'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

from api_test import TestAPI
import os
import requests


class TestOWDAppMaker292(TestAPI):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.resource = 'origins'
        self.expected_status_get = 200

    def test_get_created_origin(self):
        path = 'res/test-appcache.zip'
        url = self.root + self.resource

        size = os.path.getsize(path)
        origin_id, location = self.utils.create_resource(url, path)
        self.assertTrue(self.utils.wait_for_worker_progress(location, 100, 20), msg='Origin not created')

        # Retrieve uploaded file and check it's the same as the original
        url = '{}{}/{}'.format(self.root, self.resource, origin_id)

        res = requests.get(url).json()
        self.assertEquals('test-appcache.zip', res['src']['name'], msg='File names do not match')
        self.assertEquals('zip', res['type'], msg='File types do not match')
        self.assertEquals(size, res['src']['size'], msg='File sizes do not match')
