# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

#===============================================================================
# [DELETE] Delete a non-existing channel
#===============================================================================
from api_test import TestAPI


class TestOWDAppMaker316(TestAPI):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.resource = 'projects'

        # Create the parent project
        url = "{}{}".format(self.root, self.resource)
        payload = {'name': 'Test App cool name'}
        self.project_id, self.location = self.utils.create_project(url, payload)
        self.assertTrue(self.utils.wait_for_worker_progress(self.location, 100, 20), msg='Project not created')

    def tearDown(self):
        url = '{}{}/{}'.format(self.root, self.resource, self.project_id)
        work_id = self.utils.delete_project(url)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), "Project not deleted")

    def test_delete_channel(self):
        channel = 'NONEXIST'
        url = "{}{}/{}/channels/{}".format(self.root, self.resource, self.project_id, channel)
        self.send_request(url, 'delete', None, expected_status=404)
