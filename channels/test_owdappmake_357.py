# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
#===============================================================================
# [POST][channels]Add a channel, without origin (type production)
#===============================================================================

from api_test import TestAPI


class TestOWDAppMaker357(TestAPI):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.resource = 'projects'
        self.expected_response = "Bad parameters"

    def tearDown(self):
        url = '{}{}/{}'.format(self.root, self.resource, self.project_id)
        work_id = self.utils.delete_project(url)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), "Timeout exceeded deleting project")

    def test_add_channel_without_origin(self):
        # Creating a project
        url = self.root + self.resource
        payload = {'name': 'name'}

        self.project_id, work_id = self.utils.create_project(url, payload)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg='Origin not created')

        # Creating a channel with the info given in data and payload
        url = '{}{}/{}/{}'.format(self.root, self.resource, self.project_id, 'channels')
        path = 'res/icon.png'
        app_info = {'name': 'app_name', 'description': 'app_description', 'iconUrl': path}

        _, content = self.utils.create_channel(
            url, expected_status=412, name='PRODUCTION', deployment_type='github', app_info=app_info)
        self.assertEquals(content, self.expected_response, msg='Expected answer ({}) does not match the real one ({})'
                          .format(self.expected_response, content))
