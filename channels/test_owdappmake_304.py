# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''

#===============================================================================
# [GET] Get an existing channel
#===============================================================================
from api_test import TestAPI


class TestOWDAppMaker304(TestAPI):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.resource = 'projects'
        self.channel_name = 'PRODUCTION'
        self.deployment = 'appmaker'
        self.origin_id, self.project_id, self.channel_id = self._add_channel(self.channel_name, self.deployment)

    def tearDown(self):
        url = '{}{}/{}'.format(self.root, self.resource, self.project_id)
        work_id = self.utils.delete_project(url)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), "Timeout exceeded deleting project")

    def test_get_a_channel(self):
        url = '{}{}/{}/{}/{}'.format(self.root, self.resource, self.project_id, 'channels', self.channel_id)
        self.logger.debug('Getting the channel @ url: {}'.format(url))

        res = self.send_request(url, 'get', None, expected_status=200)
        channel_info = res.json()
        self.assertEquals('PRODUCTION', channel_info['name'], 'Wrong channel name: [{}] Expected: [{}]'.
                          format(channel_info['name'], 'PRODUCTION'))
        self.assertEquals(self.project_id, channel_info['projectId'], 'Wrong projectId: [{}] Expected [{}]'.
                          format(channel_info['projectId'], self.project_id))
        self.assertEquals(self.channel_id, channel_info['channelId'], 'Wrong channelId: [{}] Expected [{}]'.
                          format(channel_info['channelId'], self.channel_id))
        self.assertEquals('appmaker', channel_info['deployment']['type'], 'Wrong deployment type: [{}] Expected [{}]'.
                          format(channel_info['deployment']['type'], 'appmaker'))

    def _add_channel(self, channel_name, deployment_type):
        # Creating a resource
        path = 'res/test-appcache.zip'
        url = self.root + 'origins'

        origin_id, work_id = self.utils.create_resource(url, path)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg='Origin not created')

        # Creating a project
        url = self.root + self.resource
        payload = {'name': 'name'}

        project_id, work_id = self.utils.create_project(url, payload)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg='Project not created')

        # Creating a channel with the info given in data and payload
        url = '{}{}/{}/{}'.format(self.root, self.resource, project_id, 'channels')
        path = 'res/icon.png'
        app_info = {'name': 'app_name', 'description': 'app_description', 'iconUrl': path}

        channel_id, work_id = self.utils.create_channel(
            url, name=channel_name, projectId=project_id, originId=origin_id, deployment=deployment_type,
            appInfo=app_info)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), 'Channel not created')

        return origin_id, project_id, channel_id
