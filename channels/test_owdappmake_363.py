# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
#===============================================================================
# [POST][channels]Add a channel, without appInfo (type production)
#===============================================================================

from api_test import TestAPI


class TestOWDAppMaker363(TestAPI):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.resource = 'projects'
        self.expected_response = 'Bad parameters'

    def tearDown(self):
        url = '{}{}/{}'.format(self.root, self.resource, self.project_id)
        work_id = self.utils.delete_project(url)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20),
                        "Timeout exceeded deleting project")

    def test_add_channel_without_app_info_prod(self):
        # Creating a resource
        path = 'res/test-appcache.zip'
        url = self.root + 'origins'

        origin_id, work_id = self.utils.create_resource(url, path)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg='Origin not created')

        # Creating a project
        url = self.root + self.resource
        payload = {'name': 'name'}

        self.project_id, work_id = self.utils.create_project(url, payload)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg='Project not created')

        # Creating a channel with the info given in data and payload
        # TODO - Check current behavior and check what we should assert
        url = '{}{}/{}/{}'.format(self.root, self.resource, self.project_id, 'channels')
        _, content = self.utils.create_channel(
            url, expected_status=412, name='PRODUCTION', projectId=self.project_id, originId=origin_id,
            deployment='appmaker')
        self.assertEquals(content, self.expected_response)
