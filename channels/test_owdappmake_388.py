# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
#===============================================================================
# [PUT][channels] Update all channel's fields
#===============================================================================

import json
import magic
from api_test import TestAPI


class TestOWDAppMaker388(TestAPI):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.resource = 'projects'

    def tearDown(self):
        url = '{}{}/{}'.format(self.root, self.resource, self.project_id)
        work_id = self.utils.delete_project(url)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), "Timeout exceeded deleting project")

    def test_modify_appInfo_channel(self):
        # Creating a resource
        path = 'res/test-appcache.zip'
        url = self.root + 'origins'

        origin_id, work_id = self.utils.create_resource(url, path)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg='Origin not created')

        # Create a new origin to replace the old one
        path = 'res/test-appcache2.zip'
        url = self.root + 'origins'

        new_origin_id, work_id = self.utils.create_resource(url, path)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg='Origin #2 not created')

        # Creating a project
        url = self.root + self.resource
        payload = {'name': 'name3'}

        self.project_id, work_id = self.utils.create_project(url, payload)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg='Project not created')

        # Creating a channel with the info given in data and payload
        url = '{}{}/{}/{}'.format(self.root, self.resource, self.project_id, 'channels')
        path = 'res/icon.png'
        app_info = {'name': 'app_name', 'description': 'app_description', 'iconUrl': path}

        channel_id, work_id = self.utils.create_channel(
            url, name='PRODUCTION', projectId=self.project_id, originId=origin_id, deployment='appmaker',
            appInfo=app_info)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg='Channel not created')

        # Modify all channel's fields
        url = '{}{}/{}/{}/{}'.format(self.root, self.resource, self.project_id, 'channels', channel_id)

        new_icon = 'res/icon2.png'
        mime_type = magic.from_file(new_icon, mime=True)
        new_icon_url = "data:{};base64,".format(mime_type) + self.utils.icon_to_base64(new_icon)
        new_app_info = {'name': 'new_name', 'description': 'new_description', 'iconUrl': new_icon_url}
        payload['name'] = 'BETA'
        payload['tag'] = 'beta'
        payload['appInfo'] = new_app_info
        payload['originId'] = new_origin_id
        payload['deployment'] = {"type": "appmaker", "params": {"subdomain": ""}}

        res = self.send_request(url, 'put', json.dumps(payload), expected_status=202,
            expected_headers=['location'], content_type='application/json')
        work_id = res.headers['location']
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg='Channel not modified')
        channel_info = self.send_request(url, 'get', None, expected_status=200).json()

        self.logger.debug("NAME: {}".format(channel_info['tag']))
        self.utils.check_channel_field('tag', 'beta', channel_info['tag'])
        self.utils.check_channel_field('originId', new_origin_id, channel_info['originId'])
        self.utils.check_channel_field('appInfo[name]', new_app_info['name'], channel_info['appInfo']['name'])
        self.utils.check_channel_field('appInfo[description]', new_app_info['description'],
                                       channel_info['appInfo']['description'])
