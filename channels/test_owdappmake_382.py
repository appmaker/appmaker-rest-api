# -*- coding: utf-8 -*-
'''
(c) Copyright 2015 Telefonica, I+D. Printed in Spain (Europe). All Rights
Reserved.

The copyright to the software program(s) is property of Telefonica I+D.
The program(s) may be used and or copied only with the express written
consent of Telefonica I+D or in accordance with the terms and conditions
stipulated in the agreement/contract under which the program(s) have
been supplied.
'''
#===============================================================================
# [PUT][channels] Update a channel's deployment method
#===============================================================================

from api_test import TestAPI


class TestOWDAppMaker382(TestAPI):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.resource = 'projects'

    def tearDown(self):
        url = '{}{}/{}'.format(self.root, self.resource, self.project_id)
        work_id = self.utils.delete_project(url)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), "Timeout exceeded deleting project")

    def test_modify_channel_deployment(self):
        # Creating a resource
        path = 'res/test-appcache.zip'
        url = self.root + 'origins'

        origin_id, work_id = self.utils.create_resource(url, path)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg='Origin not created')

        # Creating a project
        url = self.root + self.resource
        payload = {'name': 'name'}

        self.project_id, work_id = self.utils.create_project(url, payload)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg='Project not created')

        # Creating a channel with the info given in data and payload
        url = '{}{}/{}/{}'.format(self.root, self.resource, self.project_id, 'channels')
        path = 'res/icon.png'
        app_info = {'name': 'app_name', 'description': 'app_description', 'iconUrl': path}

        channel_id, work_id = self.utils.create_channel(
            url, name='PRODUCTION', projectId=self.project_id, originId=origin_id, deployment='appmaker',
            appInfo=app_info)
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg='Channel not created')

        # Modify a channel's origin
        url = '{}{}/{}/{}/{}'.format(self.root, self.resource, self.project_id, 'channels', channel_id)
        deployment = {'type': 'github',
                      'params': {'deploymentGitHubRepository': "http://www.anotherurl.com"}}
        payload = {'deployment': deployment}

        # TODO: Review when partial payload is accepted
        res = self.send_request(url, 'put', payload, expected_status=202, expected_headers=['location'])
        work_id = res.json()['location']
        self.assertTrue(self.utils.wait_for_worker_progress(work_id, 100, 20), msg='Channel not modified. Put failed')
